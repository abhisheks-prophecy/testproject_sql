WITH goods_classification_gitlab AS (

  SELECT * 
  
  FROM {{ ref('goods_classification_gitlab')}}

),

all_type_non_partitioned AS (

  SELECT * 
  
  FROM {{ source('spark_catalog.qa_db_warehouse', 'all_type_non_partitioned') }}

),

Join_1 AS (

  SELECT 
    in0.c_tinyint AS c_tinyint,
    in0.c_smallint AS c_smallint,
    in0.c_int AS c_int,
    in0.c_bigint AS c_bigint,
    in0.c_float AS c_float,
    in0.c_double AS c_double,
    in0.c_string AS c_string,
    in0.c_boolean AS c_boolean,
    in0.c_array AS c_array,
    in0.c_struct AS c_struct
  
  FROM all_type_non_partitioned AS in0
  INNER JOIN goods_classification_gitlab AS in1
     ON in0.c_string != in1.Status_HS4

)

SELECT *

FROM Join_1
